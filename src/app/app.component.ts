import {AfterViewInit, Compiler, CompilerFactory, Component, NgModule, ViewChild, ViewContainerRef} from '@angular/core';
import {CommonModule} from '@angular/common';
import {JitCompilerFactory} from '@angular/platform-browser-dynamic';
import { getComponent } from '@angular/core/src/linker/component_factory_resolver';
import { SimulatedService } from './simualted.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'dynamic-component';
    @ViewChild('container', { read: ViewContainerRef, static: true }) container: ViewContainerRef;
    constructor(private compiler: Compiler, private simulatedService: SimulatedService) {}

    ngAfterViewInit() {
        this.compiler.clearCache();
        let component;
        this.simulatedService
        .getComponent()
        .subscribe(
          item => {
            component = Component(
              {
                template: item.html,
                styles: item.styles
              }
              )(class {
              value: any;
              constructor(private simulatedService: SimulatedService)
              {
                this.simulatedService.getValue().subscribe(
                  value => {
                    this.value = value;
                  }
                );
              }
            });
          }
        ); 


        // Define the module using NgModule decorator.
        const module = NgModule({
            declarations: [component],
            providers:[SimulatedService]
        })(class {});

        // Asynchronously (recommended) compile the module and the component.
        this.compiler.compileModuleAndAllComponentsAsync(module)
            .then(factories => {
                // Get the component factory.
                const componentFactory = factories.componentFactories[0];
                // Create the component and add to the view.
                const componentRef = this.container.createComponent(componentFactory);
            });
    }
}