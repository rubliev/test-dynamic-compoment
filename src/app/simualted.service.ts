import {Injectable} from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable()
export class SimulatedService
 {

  constructor() {
  }


  getComponent():Observable<any> {

    let valuesMap = new Map();
 
    return of({
          html: '<div>{{value?.test}} ha {{value?.test1}} anni</div>',
          styles: [':host {color: red}'],
        });
  }

  getValue():Observable<any> {

    let valuesMap = new Map();
    valuesMap.set("test", "ANDREA");
    valuesMap.set("test1", 32);
    valuesMap.set("test2", "FLOWER");
    valuesMap.set("test3", 45);
    return of({
        valuesMap
        });
  }
}